import ConfirmEvents from "./events";

const ConfirmPlugin = {
  install(Vue) {
    const confirm = (params) => {
      if (typeof params != "object" || Array.isArray(params)) {
        let caughtType = typeof params;
        if (Array.isArray(params)) caughtType = "array";

        throw new Error(
          `Options type must be an object. Caught: ${caughtType}. Expected: object`
        );
      }

      if (typeof params === "object") {
        if (
          params.hasOwnProperty("callback") &&
          typeof params.callback != "function"
        ) {
          let callbackType = typeof params.callback;
          throw new Error(
            `Callback type must be an function. Caught: ${callbackType}. Expected: function`
          );
        }
        ConfirmEvents.$emit("open", params);
      }
    };
    confirm.close = () => {
      ConfirmEvents.$emit("close");
    };

    Vue.prototype.$confirm = confirm;
    Vue["$confirm"] = confirm;
  },
};

export default ConfirmPlugin;
