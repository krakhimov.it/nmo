export default {
  server: {
    host: "0",
    // host: "localhost",
  },

  env: {
    SITE_BACKEND_URL: process.env.SITE_BACKEND_URL,
    MAIN_BACKEND_URL: process.env.MAIN_BACKEND_URL,
    UPLOADS_BACKEND_URL: process.env.UPLOADS_BACKEND_URL,
  },

  publicRuntimeConfig: {
    SITE_BACKEND_URL: process.env.SITE_BACKEND_URL,
    MAIN_BACKEND_URL: process.env.MAIN_BACKEND_URL,
    UPLOADS_BACKEND_URL: process.env.UPLOADS_BACKEND_URL,
  },

  ssr: true,

  components: true,

  head: {
    title: "ТМТ – ТАҲСИЛОТИ МУТАССИЛИ ТИББӢ",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover",
      },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      { name: "robots", content: "index, follow" },
      {
        hid: "description",
        name: "description",
        content:
          "ТАҲСИЛОТИ МУТТАСИЛӢ МУТАХАССИСОНИ ТИБ – мо  бузургтарин платформаи таълимӣ дар Тоҷикистон ҳастем, ки барои рушд ва такмили донишу малакаҳои мутахассисони тибби оилавӣ мусоидат менамояд.",
      },
      {
        hid: "og:description",
        name: "og:description",
        content: "ТМТ – ТАҲСИЛОТИ МУТАССИЛИ ТИББӢ",
      },
      {
        name: "keywords",
        content: "ТМТ – ТАҲСИЛОТИ МУТАССИЛИ ТИББӢ",
      },
      {
        hid: "og:image",
        name: "og:image",
        content: "https://rmk-tmt.tj/favicon.ico",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css",
      },
    ],
    script: [
      {
        // src: "https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit",
      },
    ],
  },

  css: ["~/assets/scss/index.scss"],

  styleResources: {
    scss: ["~assets/scss/_responsive.scss", "~assets/scss/_variables.scss"],
  },

  plugins: [
    {
      src: "~/plugins/plugins.js",
    },
    { src: "~/plugins/html2pdf", mode: "client" },
  ],

  modules: ["@nuxtjs/axios", "@nuxtjs/auth-next", "vue2-editor/nuxt"],

  buildModules: ["@nuxtjs/style-resources"],

  build: {
    scss: {
      implementation: require("sass"),
    },
    extend(config, ctx) {
      config.module.rules.push({
        enforce: "pre",
        test: /\.txt$/,
        loader: "raw-loader",
        exclude: /(node_modules)/,
      });
    },
    transpile: ["defu", "axios"],
  },

  pageTransition: "page-transition",

  axios: {
    baseURL: process.env.MAIN_BACKEND_URL,
    // baseURL: "http://nmo.mirllex.com/server",
    // baseURL: "http://localhost:3004",
    credentials: true,
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: "token",
          global: true,
          required: true,
          type: "Bearer",
        },
        endpoints: {
          login: { url: "/auth/login", method: "post" },
          user: { url: "/auth/user", method: "get" },
          // user: false,
          logout: false,
        },
      },
    },
    redirect: {
      login: "",
      logout: "/",
      home: "",
    },
  },

  serverMiddleware: [
    { path: "/api", handler: "~/serverMiddleware/api-image.js" },
    { path: "/api", handler: require("body-parser").json() },
    {
      path: "/api",
      handler: (req, res, next) => {
        const url = require("url");
        req.query = url.parse(req.url, true).query;
        req.params = { ...req.query, ...req.body };
        next();
      },
    },
  ],
};
