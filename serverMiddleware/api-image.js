import express from "express";
import bodyParser from "body-parser";
import multer from "multer";
import fs from "fs";
import e from "express";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const storageAvatar = multer.diskStorage({
  destination: "./static/avatars/",
  filename: function (req, file, cb) {
    file.originalname = Buffer.from(file.originalname, "latin1").toString(
      "utf8"
    );
    cb(null, file.originalname);
  },
});
const uploadAvatar = multer({ storage: storageAvatar });

app.post("/uploadAvatar", uploadAvatar.single("avatar"), async (req, res) => {
  res.json({ answer: "ok" });
});

app.post("/removeAvatar", async (req, res) => {
  fs.unlink(req.body.path, (err) => {});
  res.json({ answer: "ok" });
});

// Exercises
const storageExercise = multer.diskStorage({
  destination: "./static/exercises/images",
  filename: function (req, file, cb) {
    file.originalname = Buffer.from(file.originalname, "latin1").toString(
      "utf8"
    );
    cb(null, file.originalname);
  },
});
const uploadExercise = multer({ storage: storageExercise });

app.post(
  "/uploadExercise",
  uploadExercise.single("exercise"),
  async (req, res) => {
    res.json({ answer: "ok" });
  }
);

app.post("/removeExercise", async (req, res) => {
  req.body.remove_list.forEach((element) => {
    fs.unlink(element, (err) => {
      if (err) {
      }
    });
  });
  res.json({ answer: "ok" });
});

// Exercise matereials
const storageExerciseMaterials = multer.diskStorage({
  destination: "./static/exercises/materials",
  filename: function (req, file, cb) {
    file.originalname = Buffer.from(file.originalname, "latin1").toString(
      "utf8"
    );
    cb(null, file.originalname);
  },
});
const uploadExerciseMaterials = multer({ storage: storageExerciseMaterials });

app.post(
  "/uploadExerciseMaterials",
  uploadExerciseMaterials.array("exerciseMaterials"),
  async (req, res) => {
    res.json({ answer: "ok" });
  }
);

// Reports
// const storageReports = multer.diskStorage({
//   destination: "./static/assignments/reports",
//   filename: function (req, file, cb) {
//     file.originalname = Buffer.from(file.originalname, "latin1").toString(
//       "utf8"
//     );
//     cb(null, file.originalname);
//   },
// });
// const uploadReports = multer({ storage: storageReports });

// app.post("/uploadReports", uploadReports.array("reports"), async (req, res) => {
//   res.json({ answer: "ok" });
// });

// app.post("/removeReports", async (req, res) => {
//   req.body.remove_list.forEach((element) => {
//     fs.unlink(element, (err) => {});
//   });
//   res.json({ answer: "ok" });
// });

// Banner

const storageBanner = multer.diskStorage({
  destination: "./static/banner/",
  filename: function (req, file, cb) {
    file.originalname = Buffer.from(file.originalname, "latin1").toString(
      "utf8"
    );
    cb(null, file.originalname);
  },
});
const uploadBanner = multer({ storage: storageBanner });

app.post("/uploadBanner", uploadBanner.single("banner"), async (req, res) => {
  res.json({ answer: "ok" });
});

app.post("/removeBanner", async (req, res) => {
  fs.unlink(req.body.path, (err) => {});
  res.json({ answer: "ok" });
});

module.exports = app;
