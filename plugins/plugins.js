import Vue from "vue";

import vClickOutside from "v-click-outside";
Vue.use(vClickOutside);

import VueMask from "v-mask";
Vue.use(VueMask);

import ConfirmPlugin from "../components/confirm/plugin.js";
Vue.use(ConfirmPlugin);
